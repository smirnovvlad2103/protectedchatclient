package com.vlaro.application.protectedchatclient.recyclerview;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.activity.ChatActivity;
import com.vlaro.application.protectedchatclient.activity.MainActivity;
import com.vlaro.application.protectedchatclient.model.UserInfo;

import java.util.List;

public class UserRecyclerViewAdapter extends RecyclerView.Adapter<UserRecyclerViewHolders> {
    private List<UserInfo> userInfoList;
    private MainActivity activity;

    public UserRecyclerViewAdapter(List<UserInfo> userInfoList, MainActivity activity) {
        this.userInfoList = userInfoList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public UserRecyclerViewHolders onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, null);
//        layoutView.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(activity, ChatActivity.class);
//                intent.putExtra("receiver",);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//                activity.startActivity(intent);
//            }
//        });
        return new UserRecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserRecyclerViewHolders userRecyclerViewHolders, int i) {
        userRecyclerViewHolders.userName.setText(userInfoList.get(i).getUserName());
        userRecyclerViewHolders.lastMessage.setText(userInfoList.get(i).getLastMessage());
        userRecyclerViewHolders.userPhoto.setImageResource(userInfoList.get(i).getPhotoId());

        final int k = i;

        userRecyclerViewHolders.userPhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ChatActivity.class);
                intent.putExtra("receiver", userInfoList.get(k).getUserName());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.userInfoList.size();
    }
}
