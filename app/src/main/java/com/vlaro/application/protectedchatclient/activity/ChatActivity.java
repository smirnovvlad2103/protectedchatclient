package com.vlaro.application.protectedchatclient.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.model.Message;
import com.vlaro.application.protectedchatclient.model.MessageType;
import com.vlaro.application.protectedchatclient.chat.ChatManager;
import com.vlaro.application.protectedchatclient.recyclerview.MessageListRecyclerViewAdapter;
import com.vlaro.application.protectedchatclient.retrofit.RetrofitClient;
import com.vlaro.application.protectedchatclient.retrofit.service.ChatService;
import com.vlaro.application.protectedchatclient.user.UserUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class ChatActivity extends AppCompatActivity {
    private LinearLayoutManager layoutManager;
    private MessageType messageType = MessageType.MESSAGE;
    private String receiver;
    private EditText editMessage;
    private RecyclerView messageRecycler;
    private MessageListRecyclerViewAdapter messageAdapter;

    private static Handler handler;

    private Menu menu;

    private boolean isSecured = false;

    private List<Message> messageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        handler = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case 0: // set label
                        boolean isSecured = (boolean) msg.obj;
                        if (isSecured) {
                            messageType = MessageType.SECURED;
                            menu.getItem(0).setIcon(R.drawable.green);
                        } else {
                            messageType = MessageType.MESSAGE;
                            menu.getItem(0).setIcon(R.drawable.red);
                        }
                        break;

                    case 1: // todo ROMA add message to UI
                        List<Message> receivedMessageList = (List<Message>) msg.obj;

                        for (Message receivedMessage : receivedMessageList) {
                            messageList.add(receivedMessage);

                            int newPosition = messageList.size() - 1;

                            messageAdapter.notifyItemInserted(newPosition);
                            messageRecycler.scrollToPosition(newPosition);
                        }


                        break;
                }
            }
        };

        // Загружаем все сообщения для чата
        receiver = getIntent().getStringExtra("receiver");
        System.out.println("----------------------------------" + receiver);

        messageRecycler = (RecyclerView) findViewById(R.id.chat_recycler_view);
        messageAdapter = new MessageListRecyclerViewAdapter(this, messageList); // сюда передавать сообщения
        messageRecycler.setAdapter(messageAdapter);


        layoutManager = new LinearLayoutManager(this);
        messageRecycler.setLayoutManager(layoutManager);


        ChatManager.getInstance().setChatActivity(this);

        Button sendButton = findViewById(R.id.send_button);
        editMessage = findViewById(R.id.input_message);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

    }

    private void sendMessage() {
        String messageForSend = editMessage.getText().toString();
        if (!messageForSend.isEmpty()) {
            Message message = new Message(UserUtils.getInstance().getUsername(), receiver, messageForSend);
            messageList.add(message);

            int newMessagePosition = messageList.size() - 1;

            messageAdapter.notifyItemInserted(newMessagePosition);
            messageRecycler.scrollToPosition(newMessagePosition);

            editMessage.setText("");
            formMessage(messageForSend);
        }
    }

    private void formMessage(String message) {
        JSONArray mArr = new JSONArray();
        mArr.put(new Message(UserUtils.getInstance().getUsername(), receiver, message).toJSONArray());
        ChatManager.getInstance().sendMessage(mArr, messageType);

    }

    private void getAllMessageForChat() {
//        // todo: вынести в отдельный поток
//        ChatService chatService = RetrofitClient.getInstance().create(ChatService.class);
//        messageList = new ArrayList<>();
//
//        Call<List<Message>> call = chatService.getAllMessage(UserUtils.getInstance().getUsername(), receiver);
//        call.enqueue(new Callback<List<Message>>() {
//            @Override
//            public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
//                System.out.println("RESPONSE:" + response.body());
//                if (response.isSuccessful()) {
//                    System.out.println("RESPONSE:" + response.body());
//                    messageList.addAll(response.body());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Message>> call, Throwable t) {
//                System.out.println("Some error: " + t.getMessage());
//            }
//        });
        JSONArray messageArray = new JSONArray();
        messageArray.put(new Message(UserUtils.getInstance().getUsername(), receiver, "").toJSONArray());
        ChatManager.getInstance().sendMessage(messageArray, MessageType.CHAT);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.menu = menu;
        getMenuInflater().inflate(R.menu.chat_menu, menu);
        setLabelSecured(ChatManager.getInstance().getSecuredChannel(receiver) != null
                && ChatManager.getInstance().getSecuredChannel(receiver).isSecured(), receiver);

        getAllMessageForChat();

        return true;
    }

    public void setLabelSecured(boolean isSecured, String receiver) {
        if (receiver.equals(this.receiver)) {
            this.isSecured = isSecured;
            handler.sendMessage(handler.obtainMessage(0, isSecured));
        }
    }

    public void updateMessageList(List<Message> receivedMessages) {
        handler.sendMessage(handler.obtainMessage(1, receivedMessages));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.encryption:
                if (isSecured) {
                    Toast.makeText(this, "Close encrypt connection", Toast.LENGTH_SHORT).show();
                    closeSecuredChannel();
                } else {
                    Toast.makeText(this, "Init encrypt connection", Toast.LENGTH_SHORT).show();
                    addSecuredChannel();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void addSecuredChannel() {
        ChatManager.getInstance().initSecuredConnection(this, receiver);
    }

    private void closeSecuredChannel() {
        ChatManager.getInstance().closeSecuredChannel(receiver);
    }

    public String getReceiver() {
        return receiver;
    }
}
