package com.vlaro.application.protectedchatclient.user;

import com.vlaro.application.protectedchatclient.activity.LoginActivity;
import com.vlaro.application.protectedchatclient.call.LoginCall;
import com.vlaro.application.protectedchatclient.retrofit.RetrofitClient;
import com.vlaro.application.protectedchatclient.retrofit.service.LoginService;

import retrofit2.Call;

public class UserUtils {
    private long userId;
    private String username;
    private String passwordHash;

    private static UserUtils instance;

    private UserUtils() {}

    public static void init(String username) {
        instance = new UserUtils();
        instance.username = username;
    }

    public static UserUtils getInstance() {
        return instance;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return this.userId;
    }

    public String getUsername() {
        if (username != null) {
            return username;
        }
        return "";
    }

    public String getPasswordHash() {
        if (passwordHash != null) {
            return passwordHash;
        }
        return "";
    }

    public static void tryToLogin(LoginActivity activity, String username, String password) {
        RetrofitClient.init(activity, username, password);

        UserUtils.getInstance().passwordHash = password; // todo make hash

        LoginService loginService = RetrofitClient.getInstance().create(LoginService.class);
        Call<Long> call = loginService.login(username);
        call.enqueue(new LoginCall(activity, username, password));
    }
}
