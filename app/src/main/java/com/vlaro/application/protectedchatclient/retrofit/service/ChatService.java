package com.vlaro.application.protectedchatclient.retrofit.service;

import com.vlaro.application.protectedchatclient.model.Message;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ChatService {

    @GET("chat/sendMessage")
    Call<Void> sendMessage(@Query("message") String message, @Query("userId") long userId);

    @GET("chat/getMessage")
    Call<String> getMessage(@Query("userId") long userId);

    @GET("chat/getAllMessagesForChat")
    Call<List<Message>> getAllMessage(@Query("sender") String sender, @Query("receiver") String receiver);
}
