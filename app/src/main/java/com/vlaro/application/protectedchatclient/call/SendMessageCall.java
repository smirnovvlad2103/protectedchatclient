package com.vlaro.application.protectedchatclient.call;

import android.widget.EditText;
import android.widget.Toast;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.activity.MainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessageCall implements Callback<Void> {

    private MainActivity activity;

    public SendMessageCall(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onResponse(Call<Void> call, Response<Void> response) {


//        EditText message = activity.findViewById(R.id.send_message_input);
//
//        if (response.isSuccessful()) {
//            message.setText("");
//        } else {
//            Toast.makeText(activity, "Can't Send Message", Toast.LENGTH_LONG).show();
//        }

    }

    @Override
    public void onFailure(Call<Void> call, Throwable throwable) {
        Toast.makeText(activity, "Can't Reach Server", Toast.LENGTH_LONG).show();
        System.out.println("SOMETHING WRONG " + throwable.getMessage()); // todo in logs
    }
}