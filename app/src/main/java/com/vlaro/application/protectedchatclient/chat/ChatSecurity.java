package com.vlaro.application.protectedchatclient.chat;

import android.app.Activity;
import android.util.Log;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.model.Message;
import com.vlaro.application.protectedchatclient.model.MessageType;
import com.vlaro.application.protectedchatclient.model.SecurityStatus;
import com.vlaro.application.protectedchatclient.user.UserUtils;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.WebSocket;

import static com.vlaro.application.protectedchatclient.chat.ChatManager.NORMAL_CLOSURE_STATUS;

public class ChatSecurity {

    private static final Integer lines = 125_000;
    private static final Integer numbers = 8;


    public static void initiateSecurity(Activity activity, String receiver) {

        BigInteger p = getPrimeNumber(activity);
        BigInteger q = getPrimeNumber(activity);
        BigInteger a = getPrimeNumber(activity);

        BigInteger qa = powerUp(a, p, q);

        SecurityStatus securityStatus = new SecurityStatus();
        securityStatus.setP(p);
        securityStatus.setQ(q);
        securityStatus.setAb(a);


        String message = "p_" + p + "___q_" + q + "___qa_" + qa;

        JSONArray mArr = new JSONArray();
        mArr.put(new Message(UserUtils.getInstance().getUsername(), receiver, message).toJSONArray());

        ChatManager chatManager = ChatManager.getInstance();
        chatManager.sendMessage(mArr, MessageType.INIT_SECURITY);
        chatManager.openSecuredChannel(receiver, securityStatus);

    }

    private static void secondStage(Activity activity, String receiver, BigInteger p, BigInteger q, BigInteger qa) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        BigInteger b = getPrimeNumber(activity);
        BigInteger s = powerUp(b, p, qa);
        BigInteger qb = powerUp(b, p, q);

        SecurityStatus securityStatus = new SecurityStatus();
        securityStatus.setP(p);
        securityStatus.setQ(q);
        securityStatus.setAb(b);
        securityStatus.setSecretKey(createKey(s, receiver, false));

        String message = "qb_" + qb;

        JSONArray mArr = new JSONArray();
        mArr.put(new Message(UserUtils.getInstance().getUsername(), receiver, message).toJSONArray());

        ChatManager chatManager = ChatManager.getInstance();
        chatManager.sendMessage(mArr, MessageType.INIT_SECURITY);
        chatManager.openSecuredChannel(receiver, securityStatus);
    }

    private static void thirdStage(String receiver, BigInteger qb) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        ChatManager chatManager = ChatManager.getInstance();
        SecurityStatus securityStatus = chatManager.getSecuredChannel(receiver);

        BigInteger s = powerUp(securityStatus.getAb(), securityStatus.getP(), qb);
        securityStatus.setSecretKey(createKey(s, receiver, true));
        securityStatus.setSecured(true);

        String message = "success";

        JSONArray mArr = new JSONArray();
        mArr.put(new Message(UserUtils.getInstance().getUsername(), receiver, message).toJSONArray());

        chatManager.sendMessage(mArr, MessageType.INIT_SECURITY);
        chatManager.updateSecuredChannel(receiver, securityStatus);

        chatManager.updateLabelEncryption(true, receiver);
    }

    private static void forthStage(String receiver) {

        ChatManager chatManager = ChatManager.getInstance();
        SecurityStatus securityStatus = chatManager.getSecuredChannel(receiver);

        securityStatus.setSecured(true);
        chatManager.updateSecuredChannel(receiver, securityStatus);

        chatManager.updateLabelEncryption(true, receiver);
    }

    public static void determineStage(Activity activity, JSONObject messageObj, WebSocket webSocket) throws JSONException, NoSuchAlgorithmException, UnsupportedEncodingException {

        JSONObject content = messageObj.getJSONObject("content");
        Message message = Message.toMessage(content.getJSONArray("message").getJSONArray(0));

        String m = message.getPayload();
        String[] sm;
        String[] nm;

        switch (m.split("_+").length) {
            case 6:

                BigInteger p = null, q = null, qa = null;
                sm = m.split("___");
                for (String s : sm) {
                    nm = s.split("_");
                    switch (nm[0]) {
                        case "p":

                            p = new BigInteger(nm[1]);
                            break;

                        case "q":

                            q = new BigInteger(nm[1]);
                            break;

                        case "qa":

                            qa = new BigInteger(nm[1]);
                            break;

                        default:

                            Log.e("ChatManager", "something wrong");
                            webSocket.close(NORMAL_CLOSURE_STATUS, "");
                            break;
                    }
                }

                ChatSecurity.secondStage(activity, message.getSender(), p, q, qa);
                break;

            case 2:

                nm = m.split("_");
                BigInteger qb;

                if (nm[0].equals("qb")) {
                    qb = new BigInteger(nm[1]);
                    ChatSecurity.thirdStage(message.getSender(), qb);
                } else {
                    Log.e("ChatManager", "something wrong");
                    webSocket.close(NORMAL_CLOSURE_STATUS, "");
                }

                break;

            case 1:

                if (m.equals("success")) {
                    ChatSecurity.forthStage(message.getSender());
                } else {
                    Log.e("ChatManager", "something wrong");
                    webSocket.close(NORMAL_CLOSURE_STATUS, "");
                }

                break;

            default:

                Log.e("ChatManager", "something wrong");
                webSocket.close(NORMAL_CLOSURE_STATUS, "");
                break;
        }
    }

    private static BigInteger getPrimeNumber(Activity activity) {
        String line;

        try {
            InputStream inputStream = activity.getResources().openRawResource(R.raw.primes17);
            BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(inputStream));

            int lineNum = ThreadLocalRandom.current().nextInt(0, lines);
            int numNum = ThreadLocalRandom.current().nextInt(1, numbers+1);

            int i = 0;

            while ( (line = bufferedReader.readLine()) != null )
            {
                if (i++ == lineNum)
                    break;
            }
            inputStream.close();
            bufferedReader.close();

            if (line == null) {
                return null;
            }

            String[] num = line.split("\\s+");

            return new BigInteger(num[numNum]);

        } catch(IOException ex) {
            Log.d("ChatSecurity", ex.getMessage());
        }
        return null;
    }

    private static BigInteger powerUp(BigInteger a_b, BigInteger p, BigInteger q) {
        return  (a_b != null) ? q.modPow(a_b, p) : null;
    }

    private static SecretKeySpec createKey(BigInteger secretKey, String receiver, boolean isInitiator) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] key;

        if (isInitiator) {
            key = (UserUtils.getInstance().getUsername() + secretKey.toString() + receiver).getBytes("UTF-8");
        } else {
            key = (receiver + secretKey.toString() + UserUtils.getInstance().getUsername()).getBytes("UTF-8");
        }
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // get only first 128 bytes

        return new SecretKeySpec(key, "AES");
    }

    public static Message encrypt(Message message, Cipher cipher) throws BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {

        byte[] encrypted = cipher.doFinal((message.getPayload()).getBytes("UTF-8"));

        message.setPayload(new String(Base64.encodeBase64(encrypted)));

        return message;
    }

    public static Message decrypt(Message message, Cipher cipher) throws BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {

        byte[] decrypted = cipher.doFinal(Base64.decodeBase64(message.getPayload().getBytes("UTF-8")));

//        byte[] decrypted = cipher.doFinal((message.getPayload()).getBytes("UTF-8"));

        message.setPayload(new String(decrypted));

        return message;
    }
}
