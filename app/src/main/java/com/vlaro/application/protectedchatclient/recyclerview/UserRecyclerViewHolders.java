package com.vlaro.application.protectedchatclient.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vlaro.application.protectedchatclient.R;

public class UserRecyclerViewHolders extends RecyclerView.ViewHolder {
    public TextView userName;
    public TextView lastMessage;
    public ImageView userPhoto;


    public UserRecyclerViewHolders(@NonNull View itemView) {
        super(itemView);

        userName = (TextView) itemView.findViewById(R.id.tv_username);
        lastMessage = (TextView) itemView.findViewById(R.id.tv_last_message);
        userPhoto = (ImageView) itemView.findViewById(R.id.civ_photo);
    }
}
