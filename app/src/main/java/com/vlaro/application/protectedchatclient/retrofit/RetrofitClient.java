package com.vlaro.application.protectedchatclient.retrofit;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vlaro.application.protectedchatclient.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Credentials;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private volatile static Retrofit retrofit;

    private volatile static OkHttpClient okClient;

    private volatile static SSLContext sslContext;

    private RetrofitClient() {
    }

    public static void init(Context context, String username, String password) {
        retrofit = null;
        okClient = null;

        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(5);

        try {
            KeyStore keyStore;
            keyStore = KeyStore.getInstance("PKCS12");

            InputStream caInput = context.getResources().openRawResource(R.raw.keystore);
            keyStore.load(caInput, "password".toCharArray());


            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, "password".toCharArray());
            KeyManager[] km = kmf.getKeyManagers();

            TrustManagerFactory tmFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
//                TrustManager x509wrapped = tmFactory.getX509TrustManager(tmFactory);
            tmFactory.init(keyStore);
            TrustManager[] tm = tmFactory.getTrustManagers();

            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(km, tm, new java.security.SecureRandom());

            String authToken = "";

            if (!TextUtils.isEmpty(username)
                    && !TextUtils.isEmpty(password)) {
                authToken = Credentials.basic(username, password);
            }

            AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);

            okClient = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS) // todo make constance
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
//                        .authenticator(tokenAuthenticator)
//                    .addInterceptor(tokenInterceptor)
                    .addInterceptor(interceptor)
                    .dispatcher(dispatcher)
                    .sslSocketFactory(sslContext.getSocketFactory(), (X509TrustManager) tm[0])
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String s, SSLSession sslSession) {
                            System.out.println(s + "\n" + sslSession.getProtocol());
                            return true;
                        }
                    })
                    .build();

            Gson gson = new GsonBuilder() // todo: for what it
                    .serializeSpecialFloatingPointValues()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://10.0.2.2:8070/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okClient)
                    .build();
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableKeyException | KeyManagementException e) {
            e.printStackTrace(); // todo in logs and make separate
        }
    }

    public static Retrofit getInstance() {
//        if (retrofit == null) {
//            // my token authenticator, I will add this class at below
////            TokenAuthenticator tokenAuthenticator = new TokenAuthenticator();
//
//            // I am also using interceptor which controls token if expired
//            // lets look at this scenario : My token needs to refresh after 10 hours
//            // but I came to application after 50 hours and tried to make request.
//            // of course my token is invalid and it will return 401
//            // so this interceptor checks time and refreshes token immediately before making request
//            // then continues request with refreshed token
//            // So I do not get any 401 response. But if this fails and I get 401 then my TokenAuthenticator do his job.
//            // if my TokenAuthenticator fails too, basically I just logout user and tell him to re-login.
////            TokenInterceptor tokenInterceptor = new TokenInterceptor(); // todo implement some day
//
//            // this is the critical point that helped me a lot.
//            // we using only one retrofit instance in our application
//            // and it uses this dispatcher which can only do 1 request at the same time
//
//            // the docs says : Set the maximum number of requests to execute concurrently.
//            // Above this requests queue in memory, waiting for the running calls to complete.
//
//            // we are using this OkHttp as client, you can add authenticator, interceptors, dispatchers,
//            // logging etc. easily for all your requests just editing this OkHttp client
//
//        }
        return retrofit;
    }

    public static OkHttpClient getOkClient() {
        return okClient;
    }

    public static Socket createSocket(InetAddress ipAddr, int port) {
        try {
            return sslContext.getSocketFactory().createSocket(ipAddr, port);
        } catch (IOException e) {
            Log.e("Retrofit", "Cannot create socket");
        }
        return null;
    }

    public static void cleanUp() {
        retrofit = null;
        okClient = null;
    }
}

