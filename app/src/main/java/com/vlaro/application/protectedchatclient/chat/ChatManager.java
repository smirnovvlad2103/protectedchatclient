package com.vlaro.application.protectedchatclient.chat;

import com.vlaro.application.protectedchatclient.activity.ChatActivity;
import com.vlaro.application.protectedchatclient.activity.MainActivity;
import com.vlaro.application.protectedchatclient.model.Message;
import com.vlaro.application.protectedchatclient.model.MessageType;
import com.vlaro.application.protectedchatclient.model.SecurityStatus;
import com.vlaro.application.protectedchatclient.retrofit.RetrofitClient;
import com.vlaro.application.protectedchatclient.service.sockets.ChatWebSocketListener;
import com.vlaro.application.protectedchatclient.user.UserUtils;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import okhttp3.Request;
import okhttp3.WebSocket;


public class ChatManager {
    private static ChatManager instance;
    private WebSocket ws;
    private MessageType status;
    private ConcurrentHashMap<String, SecurityStatus> channelKeys;
    static final int NORMAL_CLOSURE_STATUS = 1000;

    private MainActivity activity;
    private ChatActivity chatActivity;


    private ChatManager() {
        channelKeys = new ConcurrentHashMap<>();
    }

    public static ChatManager getInstance() {
        if (instance == null)
            instance = new ChatManager();

        return instance;
    }

    public void setChatActivity(ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
    }

    public String getCurrentReceiver() {
        return chatActivity.getReceiver();
    }

    public void updateLabelEncryption(boolean isSecured, String receiver) {
        if (chatActivity != null) {
            chatActivity.setLabelSecured(isSecured, receiver);
        }
    }

    private void notifyChatActivityAboutReceivedMessages(JSONArray messages) throws JSONException {
        List<Message> messageList = new ArrayList<>();
        for (int i = 0; i < messages.length(); i++) {
            messageList.add(Message.toMessage(messages.getJSONArray(i)));
        }
        if (!messageList.isEmpty() && (messageList.get(0).getSender().equals(chatActivity.getReceiver()) || messageList.get(0).getReceiver().equals(chatActivity.getReceiver()))) {
            chatActivity.updateMessageList(messageList);
        }
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    public void connectWebSocket() {
        Request request = new Request.Builder().url("wss://10.0.2.2:8070/connect").build();
        ChatWebSocketListener listener = new ChatWebSocketListener();
        ws = RetrofitClient.getOkClient().newWebSocket(request, listener);

        sendMessage(new JSONArray(), MessageType.INIT);
    }

    public void initSecuredConnection(ChatActivity activity, String receiver) {
        ChatSecurity.initiateSecurity(activity, receiver);
    }

    public void openSecuredChannel(String receiver, SecurityStatus key) {
        channelKeys.put(receiver, key);
    }

    public void updateSecuredChannel(String receiver, SecurityStatus key) {
        channelKeys.put(receiver, key);
    }

    public SecurityStatus getSecuredChannel(String receiver) {
        return channelKeys.get(receiver);
    }

    public void closeSecuredChannel(String receiver) {
        sendMessage(new JSONArray().put(new Message(UserUtils.getInstance().getUsername(), receiver,
                "close secured connection").toJSONArray()), MessageType.CLOSE_SECURITY);
        channelKeys.remove(receiver);
        updateLabelEncryption(false, receiver);
    }

    public void dropAllChannels() {
        channelKeys = new ConcurrentHashMap<>();
    }

    public void sendMessage(JSONArray message, MessageType messageType) {
        try {
            JSONArray updMessage = new JSONArray(message.toString());
            switch (messageType) {
                case SECURED:
                    updMessage = new JSONArray();
                    Cipher cipher = Cipher.getInstance("AES");
                    cipher.init(Cipher.ENCRYPT_MODE, getSecuredChannel(Message.toMessage(message.getJSONArray(0)).getReceiver()).getSecretKey());
                    for (int i = 0; i < message.length(); i++) {
                        Message m = Message.toMessage(message.getJSONArray(i));
                        updMessage.put(ChatSecurity.encrypt(m, cipher).toJSONArray());
                    }
                case CHAT:
                case INIT:
                case INIT_SECURITY:
                case CLOSE_SECURITY:
                case MESSAGE:
                        JSONObject messageJson = new JSONObject();
                        JSONObject content = new JSONObject();
                        content.put("message", updMessage);
                        content.put("sender_receiver", UserUtils.getInstance().getUsername());
                        messageJson.put("content", content);
                        messageJson.put("type", messageType.getDescription().toUpperCase());
                        signMessage(messageJson);
                        ws.send(messageJson.toString());


                    break;
            }
        } catch (JSONException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
            e.printStackTrace(); // todo in log
        }

    }

    public void processMessage(WebSocket webSocket, String text) {
        try {
            JSONObject messageObj = new JSONObject(text);
            if (checkSign(messageObj)) {
                JSONObject content = messageObj.getJSONObject("content");
                JSONArray messages = content.getJSONArray("message");
                JSONArray updMessages = new JSONArray(messages.toString());


                switch (MessageType.valueOf(messageObj.getString("type"))) {

                    case SECURED:
                        updMessages = new JSONArray();
                        Cipher cipher = Cipher.getInstance("AES");
                        cipher.init(Cipher.DECRYPT_MODE, getSecuredChannel(Message.toMessage(messages.getJSONArray(0)).getSender()).getSecretKey());
                        for (int i = 0; i < messages.length(); i++) {
                            Message m = Message.toMessage(messages.getJSONArray(i));
                            updMessages.put(ChatSecurity.decrypt(m, cipher).toJSONArray());
                        }
                    case CHAT:
                    case MESSAGE:

                        System.out.println(messageObj);
                        System.out.println(updMessages);
                        notifyChatActivityAboutReceivedMessages(updMessages);
                        break;

                    case APPROVED:

                        status = MessageType.CONNECTED;
                        System.out.println("--------" + messageObj);

                        break;

                    case ERROR:

                        webSocket.close(NORMAL_CLOSURE_STATUS, "");
                        break;

                    case INIT_SECURITY:

                        ChatSecurity.determineStage(activity, messageObj, webSocket);

                        break;

                    case ERROR_SECURITY:
                    case CLOSE_SECURITY:

                        String sender = Message.toMessage(messages.getJSONArray(0)).getSender();
                        channelKeys.remove(sender);
                        updateLabelEncryption(false, sender);

                        break;

                }
            } else {
                webSocket.close(NORMAL_CLOSURE_STATUS, "");
            }


        } catch (JSONException | NoSuchAlgorithmException | UnsupportedEncodingException | BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            e.printStackTrace(); // todo in log
        }
    }

    private void signMessage(JSONObject messageJson) throws JSONException, UnsupportedEncodingException, NoSuchAlgorithmException {
        JSONObject content = messageJson.getJSONObject("content");

        String toSign = content.getJSONArray("message") + content.getString("sender_receiver")
                + UserUtils.getInstance().getPasswordHash() + messageJson.getString("type");

        toSign = toSign.replaceAll("[/\\\\]","");

        byte[] bytes = toSign.getBytes("UTF-8");

        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        bytes = sha.digest(bytes);
        bytes = Arrays.copyOf(bytes, 16);

        messageJson.put("signature", new String(Base64.encodeBase64(bytes)));
    }

    private boolean checkSign(JSONObject messageJson) throws JSONException, UnsupportedEncodingException, NoSuchAlgorithmException {
        JSONObject content = messageJson.getJSONObject("content");

        String toSign = content.getJSONArray("message") + content.getString("sender_receiver")
                + UserUtils.getInstance().getPasswordHash() + messageJson.getString("type");

        toSign = toSign.replaceAll("[/\\\\]","");

        byte[] bytes = toSign.getBytes("UTF-8");

        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        bytes = sha.digest(bytes);
        bytes = Arrays.copyOf(bytes, 16);

        return messageJson.getString("signature").equals(new String(Base64.encodeBase64(bytes)));
    }

}