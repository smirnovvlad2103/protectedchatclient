package com.vlaro.application.protectedchatclient.model;

import java.math.BigInteger;

import javax.crypto.spec.SecretKeySpec;

public class SecurityStatus {

    private BigInteger p;
    private BigInteger q;
    private BigInteger ab;

    private SecretKeySpec secretKey;

    private boolean secured = false;

    public BigInteger getP() {
        return p;
    }

    public void setP(BigInteger p) {
        this.p = p;
    }

    public BigInteger getQ() {
        return q;
    }

    public void setQ(BigInteger q) {
        this.q = q;
    }

    public BigInteger getAb() {
        return ab;
    }

    public void setAb(BigInteger ab) {
        this.ab = ab;
    }

    public SecretKeySpec getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(SecretKeySpec secretKey) {
        this.secretKey = secretKey;
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }
}
