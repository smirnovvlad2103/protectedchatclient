package com.vlaro.application.protectedchatclient.listener.onclick;

import android.view.View;
import android.widget.TextView;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.activity.LoginActivity;
import com.vlaro.application.protectedchatclient.retrofit.RetrofitClient;
import com.vlaro.application.protectedchatclient.user.UserUtils;

public class LoginCL implements View.OnClickListener {

    private LoginActivity activity;

    public LoginCL(LoginActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        TextView username = activity.findViewById(R.id.login_username);
        TextView password = activity.findViewById(R.id.login_password);

        String str_username = username.getText().toString();
        String str_password = password.getText().toString();

        RetrofitClient.init(activity, str_username, str_password);

        UserUtils.init(str_username);

        UserUtils.tryToLogin(activity, str_username, str_password);
    }



}