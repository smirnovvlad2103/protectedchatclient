package com.vlaro.application.protectedchatclient.call;

import android.content.Intent;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.activity.LoginActivity;
import com.vlaro.application.protectedchatclient.activity.MainActivity;
import com.vlaro.application.protectedchatclient.user.UserUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginCall implements Callback<Long> {

    private LoginActivity activity;
    private String username;
    private String password;

    public LoginCall(LoginActivity activity, String username, String password) {
        this.activity = activity;
        this.username = username;
        this.password = password;
    }

    @Override
    public void onResponse(Call<Long> call, Response<Long> response) {

        if (response.isSuccessful()) {

            activity.setMessage(R.string.login_message_success, R.color.green);
            Long userId = response.body();
            UserUtils.getInstance().setUserId(userId);

            Intent intent = new Intent(activity, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            activity.finish();


        } else {
            if (response.code() == 401) {
                System.out.println("Bad Credentials");
                activity.setMessage(R.string.login_message_bad_credentials, R.color.red);
            }
            if (response.code() == 403) {
                System.out.println("Bad Role");
                activity.setMessage(R.string.login_message_bad_credentials, R.color.red);
            }
            if (response.code() == 404) {
                System.out.println("Not Found");
                activity.setMessage(R.string.login_message_bad_connection, R.color.red);
            }
        }
    }

    @Override
    public void onFailure(Call<Long> call, Throwable throwable) {
        activity.setMessage(R.string.login_message_bad_connection, R.color.red);
        System.out.println("SOMETHING WRONG " + throwable.getMessage()); // todo in logs
        System.out.println(call.isCanceled() + ", " + call.isExecuted());
    }
}
