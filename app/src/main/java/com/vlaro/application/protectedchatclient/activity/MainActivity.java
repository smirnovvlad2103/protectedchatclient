package com.vlaro.application.protectedchatclient.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.recyclerview.UserRecyclerViewAdapter;
import com.vlaro.application.protectedchatclient.model.UserInfo;
import com.vlaro.application.protectedchatclient.chat.ChatManager;
import com.vlaro.application.protectedchatclient.user.UserUtils;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // todo: переименовать название лэйаута на activity_main и удалить старый лэйаут
        setContentView(R.layout.activity_main);

        List<UserInfo> rowListItem = getAllItemsList();
        layoutManager = new LinearLayoutManager(MainActivity.this);
        RecyclerView recyclerView = findViewById(R.id.main_recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        UserRecyclerViewAdapter adapter = new UserRecyclerViewAdapter(rowListItem, MainActivity.this);
        recyclerView.setAdapter(adapter);

        ChatManager.getInstance().setActivity(this);
        ChatManager.getInstance().connectWebSocket();
    }


    private List<UserInfo> getAllItemsList() {
        List<UserInfo> allItems = new ArrayList<>();
        switch (UserUtils.getInstance().getUsername()) {

            case "vlad":

                allItems.add(new UserInfo("roma", "Hello!", R.drawable.photo3));
                allItems.add(new UserInfo("tanya", "Where?", R.drawable.photo2));
                allItems.add(new UserInfo("liza", "Hello!", R.drawable.photo3));
                break;

            case "roma":
                allItems.add(new UserInfo("vlad", "Hello!", R.drawable.photo3));
                allItems.add(new UserInfo("tanya", "Where?", R.drawable.photo2));
                allItems.add(new UserInfo("liza", "Hello!", R.drawable.photo3));
                break;

            case "tanya":
                allItems.add(new UserInfo("vlad", "Hello!", R.drawable.photo3));
                allItems.add(new UserInfo("roma", "Where?", R.drawable.photo2));
                allItems.add(new UserInfo("liza", "Hello!", R.drawable.photo3));
                break;

            case "liza":
                allItems.add(new UserInfo("vlad", "Hello!", R.drawable.photo3));
                allItems.add(new UserInfo("roma", "Where?", R.drawable.photo2));
                allItems.add(new UserInfo("tanya", "Where?", R.drawable.photo2));
                break;
        }

        return allItems;
    }
}
