package com.vlaro.application.protectedchatclient.retrofit.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LoginService {

    @GET("user/login")
    Call<Long> login(@Query("username") String username);
}