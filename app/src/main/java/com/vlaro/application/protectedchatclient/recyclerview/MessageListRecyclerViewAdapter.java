package com.vlaro.application.protectedchatclient.recyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.model.Message;
import com.vlaro.application.protectedchatclient.user.UserUtils;

import java.util.List;

public class MessageListRecyclerViewAdapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_MESSAGE_SEND = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private Context context;
    private List<Message> messageList;

    public MessageListRecyclerViewAdapter(Context context, List<Message> messageList) {
        this.context = context;
        this.messageList = messageList;
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messageList.get(position);

        if (message.getSender().equals(UserUtils.getInstance().getUsername())) {
            return VIEW_TYPE_MESSAGE_SEND;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }



    private class SendMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        public SendMessageHolder(@NonNull View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.send_text_message_body);
            timeText = itemView.findViewById(R.id.send_text_message_time);
        }

        void bind(Message message) {
            messageText.setText(message.getPayload());
            timeText.setText("11:40"); // Заглушка, т.к в модели первоначально не предусмотрели хранение даты.
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;

        public ReceivedMessageHolder(@NonNull View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.receive_text_message_body);
            timeText = itemView.findViewById(R.id.receive_text_message_time);
        }

        void bind(Message message) {
            messageText.setText(message.getPayload());
            timeText.setText("11:40"); // Заглушка, т.к в модели первоначально не предусмотрели хранение даты.
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SEND) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_message_send, viewGroup, false);
            return new SendMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_message_received, viewGroup, false);
            return new ReceivedMessageHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Message message = messageList.get(position);

        switch (viewHolder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SEND:
                ((SendMessageHolder) viewHolder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) viewHolder).bind(message);
                break;

        }
    }
}
