package com.vlaro.application.protectedchatclient.model;

public class UserInfo {
    private String userName;
    private String lastMessage;
    private int photoId;

    public UserInfo(String userName, String lastMessage, int photoId) {
        this.userName = userName;
        this.lastMessage = lastMessage;
        this.photoId = photoId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
