package com.vlaro.application.protectedchatclient.call;

import android.widget.TextView;
import android.widget.Toast;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.activity.MainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetMessageCall implements Callback<String> {

    private MainActivity activity;

    public GetMessageCall(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {

//        TextView textView = activity.findViewById(R.id.get_message_text);

//        if (response.isSuccessful()) {
//            textView.setText(response.body());
//            System.out.println(response.body());
//        } else {
//            textView.setText("");
//            Toast.makeText(activity, "Can't Get Message", Toast.LENGTH_LONG).show();
//        }

    }

    @Override
    public void onFailure(Call<String> call, Throwable throwable) {
        Toast.makeText(activity, "Can't Reach Server", Toast.LENGTH_LONG).show();
        System.out.println("SOMETHING WRONG " + throwable.getMessage()); // todo in logs
    }
}