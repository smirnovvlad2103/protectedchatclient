package com.vlaro.application.protectedchatclient.activity;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.vlaro.application.protectedchatclient.R;
import com.vlaro.application.protectedchatclient.listener.onclick.LoginCL;

public class LoginActivity extends AppCompatActivity {

    private TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button button = findViewById(R.id.login_button);
        message = findViewById(R.id.login_message);

        button.setOnClickListener(new LoginCL(this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void setMessage(int messageResourceId, int colorId) {
        message.setText(messageResourceId);
        message.setTextColor(ContextCompat.getColor(this, colorId));
    }
}