package com.vlaro.application.protectedchatclient.service.sockets;

import android.util.Log;
import com.vlaro.application.protectedchatclient.chat.ChatManager;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public final class ChatWebSocketListener extends WebSocketListener {
    private static final int NORMAL_CLOSURE_STATUS = 1000;

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
//        webSocket.send("Hello, it's SSaurel !");
//        webSocket.send("What's up ?");
//        webSocket.send(ByteString.decodeHex("deadbeef"));
//        webSocket.close(NORMAL_CLOSURE_STATUS, "Goodbye !");
        System.out.println("CONNNNNNNNEEEECCCTT");
    }
    @Override
    public void onMessage(WebSocket webSocket, String text) {
        Log.d("Receiving : ", text);

        ChatManager.getInstance().processMessage(webSocket, text);
    }
    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.d("Receiving bytes : ", bytes.hex());
    }
    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        ChatManager.getInstance().dropAllChannels();
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        Log.d("Closing : " + code + " / " + reason, "something");
        ChatManager chatManager = ChatManager.getInstance();
        chatManager.updateLabelEncryption(false, chatManager.getCurrentReceiver());
        chatManager.connectWebSocket();
    }
    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
//        ChatManager.getInstance().dropAllChannels();
        Log.d("Error : " + t.getMessage(), "some");
    }
}